require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET#show" do
    let!(:taxonomy1) { create :taxonomy, name: "Categories" }
    let!(:taxonomy2) { create :taxonomy, name: "Brand" }
    let(:taxon1) { create :taxon, taxonomy: taxonomy1, parent: taxonomy1.root, name: "Bugs" }
    let(:taxon2) { create :taxon, taxonomy: taxonomy2, parent: taxonomy2.root, name: "ruby" }
    let(:products1) { create_list(:product, 2, taxons: [taxon1]) }
    let(:product2) { create :product, taxons: [taxon2] }

    before do
      get :show, params: { id: taxon1.id }
    end

    it "render show" do
      expect(response).to render_template('show')
    end

    it "response successfully" do
      expect(response).to be_success
    end

    it "returns a 200 response" do
      expect(response.status).to eq(200)
    end

    it "assigns @taxon to taxon1" do
      expect(assigns(:taxon)).to eq taxon1
    end

    it "assigns @products to products1" do
      expect(assigns(:products)).to match_array products1
    end

    it "does not include other_taxon_product2" do
      expect(assigns(:products)).not_to include product2
    end

    it "assigns @taxonomies to taxonomies" do
      expect(assigns(:taxonomies)).to match_array [taxonomy1, taxonomy2]
    end
  end
end
