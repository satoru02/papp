require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET#show" do
    let(:product) { create :product }

    before do
      get :show, params: { id: product.id }
    end

    it "returns a 200 response" do
      expect(response.status).to eq(200)
    end

    it "display show.html" do
      expect(response).to render_template :show
    end

    it "returns @product to product" do
      expect(assigns(:product)).to eq product
    end
  end

  describe "@related_products" do
    let(:taxon1) { create :taxon }
    let(:taxon2) { create :taxon }
    let(:taxon3) { create :taxon }
    let(:product) { create :product, taxons: [taxon1, taxon2] }
    let(:other_product) { create :product, taxons: [taxon3] }

    before do
      get :show, params: { id: product.id }
    end

    context "returns related_products" do
      let!(:related_products) { create_list(:product, 4, taxons: [taxon1, taxon2]) }

      it "dose not include self_product" do
        expect(assigns(:related_products)).not_to include product
      end

      it "does not include other_product" do
        expect(assigns(:related_products)).not_to include other_product
      end

      it "includes same_taxon_products" do
        expect(assigns(:related_products)).to match_array related_products
      end
    end

    describe "the numbers of @related_products" do
      context "the numbers of related_products is 4" do
        let!(:related_products) { create_list(:product, 4, taxons: [taxon1, taxon2]) }

        it "returns four related_products" do
          expect(assigns(:related_products).count).to eq 4
        end
      end

      context "the numbers of related_products is over_limts" do
        let!(:related_products) { create_list(:product, 5, taxons: [taxon1, taxon2]) }

        it "not returns five related_products" do
          expect(assigns(:related_products).count).to eq 4
        end
      end
    end
  end
end
