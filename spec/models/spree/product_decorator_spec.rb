require 'rails_helper'

RSpec.describe "Spree::Product", type: :model do
  describe "#related_products" do
    subject { Spree::Product.related_products(rails_tote) }

    let(:bag) { create :taxon, name: "bag" }
    let(:ruby) { create :taxon, name: "ruby" }
    let(:rails_tote) { create :product, name: "ruby on rails tote", taxons: [bag, ruby] }
    let(:rails_bag) { create :product, name: "ruby on rails bag", taxons: [bag, ruby] }

    context "returns products with self_product" do
      it "includes rails_bag" do
        is_expected.to include rails_bag
      end
    end

    context "returns products without self_product" do
      it "dose not include rails_tote" do
        is_expected.not_to include rails_tote
      end
    end
  end
end
