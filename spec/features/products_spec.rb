require 'rails_helper'

RSpec.feature "Products", type: :feature do
  describe "product_page" do
    given(:bag) { create :taxon, name: "BAG" }
    given(:ruby) { create :taxon, name: "RUBY" }
    given!(:ruby_bag) { create :product, name: "RUBY BUG", price: 19.00, taxons: [bag, ruby] }
    given!(:ruby_tote) { create :product, name: "RUBY TOTE", taxons: [bag, ruby] }
    given!(:ruby_kaban) { create :product, name: "RUBY KABAN", taxons: [bag, ruby] }

    background do
      visit potepan_product_path(ruby_bag.id)
    end

    scenario "product_page shows self_product_details" do
      expect(page).to have_current_path(potepan_product_path(ruby_bag.id))
      expect(page).to have_title(ruby_bag.name + " | BIGBAG")
      expect(page).to have_selector ".media-body h2", text: ruby_bag.name
      expect(page).to have_selector ".media-body h3", text: ruby_bag.price
    end

    scenario "product_page includes related_products" do
      expect(page).to have_current_path(potepan_product_path(ruby_bag.id))
      expect(page).to have_title(ruby_bag.name + " | BIGBAG")
      expect(page).to have_selector ".productBox h5", text: ruby_tote.name
      expect(page).to have_selector ".productBox h5", text: ruby_kaban.name
    end

    scenario "works with related_products_link" do
      expect(page).to have_current_path(potepan_product_path(ruby_bag.id))
      click_on ruby_tote.name
      expect(page).to have_current_path(potepan_product_path(ruby_tote.id))
      expect(page).to have_title(ruby_tote.name + " | BIGBAG")
      expect(page).to have_selector ".media-body h2", text: ruby_tote.name
    end

    scenario "related_products not include self_product " do
      expect(page).to have_current_path(potepan_product_path(ruby_bag.id))
      expect(page).to have_title(ruby_bag.name + " | BIGBAG")
      expect(page).not_to have_selector ".productBox h5", text: ruby_bag.name
    end

    scenario "works with index_home_link" do
      expect(page).to have_current_path(potepan_product_path(ruby_bag.id))
      within ".pageHeader" do
        click_link "Home"
      end
      expect(page).to have_current_path(potepan_path)
    end

    scenario "works with index_logo_link" do
      expect(page).to have_current_path(potepan_product_path(ruby_bag.id))
      click_on "Logo"
      expect(page).to have_current_path(potepan_path)
    end
  end
end
