require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  describe "cateogries_page" do
    given(:taxonomy1) { create :taxonomy, name: "Categories" }
    given(:taxon1) { create :taxon, taxonomy: taxonomy1, parent: taxonomy1.root, name: "Bags" }
    given(:taxon2) { create :taxon, taxonomy: taxonomy1, parent: taxonomy1.root, name: "Mugs" }
    given!(:product1) { create :product, name: "RUBY ON RAILS TOTE", price: 15, taxons: [taxon1] }
    given!(:product2) { create :product, name: "RUBY ON RAILS MUG", price: 13, taxons: [taxon2] }

    background do
      visit potepan_category_path(taxon1.id)
    end

    scenario "visit categories_taxons_page" do
      expect(page).to have_current_path(potepan_category_path(taxon1.id))
      expect(page).to have_title(taxon1.name + " | BIGBAG")
      expect(page).to have_selector ".productBox h5", text: product1.name
      expect(page).to have_selector ".productBox h3", text: product1.price
      click_on taxon2.name
      expect(page).to have_current_path(potepan_category_path(taxon2.id))
      expect(page).to have_title(taxon2.name + " | BIGBAG")
      expect(page).to have_selector ".productBox h5", text: product2.name
      expect(page).to have_selector ".productBox h3", text: product2.price
      expect(page).not_to have_selector ".productBox h5", text: product1.name
      expect(page).not_to have_selector ".productBox h3", text: product1.price
    end

    scenario "visit product_page" do
      click_on product1.name
      expect(page).to have_current_path(potepan_product_path(product1.id))
      expect(page).to have_title(product1.name + " | BIGBAG")
      expect(page).to have_selector ".media-body h2", text: product1.name
      expect(page).to have_selector ".media-body h3", text: product1.price
    end

    scenario "works with index_home_link" do
      within ".pageHeader" do
        click_link "Home"
      end
      expect(page).to have_current_path(potepan_path)
    end

    scenario "works with index_logo_link" do
      click_on "Logo"
      expect(page).to have_current_path(potepan_path)
    end
  end
end
