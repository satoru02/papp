class Potepan::CategoriesController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :render_404
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.in_taxon(@taxon).include_images_prices
    @taxonomies = Spree::Taxonomy.includes(root: :children)
  end
end
